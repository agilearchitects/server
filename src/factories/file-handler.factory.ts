import * as fs from "fs";
import * as path from "path";
import mimeTypes from "mime-types";
import { handlerMethod, headers } from "../http-handler";
import { fileHttpHandler, firstFileHttpHandler, staticContentHttpHandler } from "../http-handler/file.http-handler";

export const fileHandlerFactory = (paths: string, headers: headers = {}): handlerMethod => fileHttpHandler(paths, headers, fs, path, mimeTypes);
export const firstFileHandlerFactory = (paths: string[], headers: headers = {}): handlerMethod => firstFileHttpHandler(paths, headers, fs, path, mimeTypes);
export const staticContentHandlerFactory = (root: string): handlerMethod => staticContentHttpHandler(root, fs, path, mimeTypes); 