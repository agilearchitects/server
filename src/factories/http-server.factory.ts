import { IAPIGatewayProxyEvent, IAPIGatewayProxyResult, lambaHandler } from "../handlers/lambda.handler";
import { IHttp, netHandler } from "../handlers/net.handler";
import { handlerMethod } from "../http-handler";

interface IServerOptions {
  type: "net" | "lambda";
  handlers: handlerMethod[];
}
interface INetServerOptions extends IServerOptions {
  type: "net";
  http: IHttp;
}
interface ILambdaServerOptions extends IServerOptions {
  type: "lambda";
}

export type serverFactoryNetResponse = { listen: (port: number, callback?: () => void) => void, onError: (callback: (error: unknown) => void) => void, close: (callback?: (error: unknown) => void) => void }; 
export type serverFactoryLamdaResponse = { handler: (event: IAPIGatewayProxyEvent) => Promise<IAPIGatewayProxyResult>, onError: (callback: (error: unknown) => void) => void };
export function serverFactory(options: INetServerOptions): serverFactoryNetResponse;
export function serverFactory(options: ILambdaServerOptions): serverFactoryLamdaResponse;
export function serverFactory(options: INetServerOptions | ILambdaServerOptions): serverFactoryNetResponse | serverFactoryLamdaResponse {
  let errorCallback: ((error: unknown) => void) | undefined;
  
  const { type, handlers } = options;
  if(type === "net") {
    const { http } = options;
    const server = netHandler({ http, onError: (error: unknown) => { if(errorCallback !== undefined) { errorCallback(error); } } }, ...handlers);
    return {
      listen: (port: number, callback?: () => void) => { server.listen(port, callback); },
      close: (callback?: (error: unknown) => void) => server.close(callback),
      onError: (callback: (error: unknown) => void) => { errorCallback = callback; }
    };
  }
  return {
    handler: (event: IAPIGatewayProxyEvent) => lambaHandler({ onError: (error: unknown) => { if(errorCallback !== undefined) { errorCallback(error); } } }, ...handlers)(event),
    onError: (callback: (error: unknown) => void) => { errorCallback = callback; }
  };
}