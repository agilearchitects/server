import { DTOFileToSchemaParserModule, ITs } from "../modules/dto-file-to-schema-parser.module";
import { schemasInput } from "../modules/object-to-schema-parser.module";

export const dtoFileToSchemaParserModuleFactory = (dtoRoot: string, ts: ITs): {
  parseSchemas: (schema: string[]) => schemasInput,
} => {
  const dtoFileToSchemaParserModule = new DTOFileToSchemaParserModule(dtoRoot, ts);
  return {
    parseSchemas: (schemas: string[]) => dtoFileToSchemaParserModule.schemasToSchemaInput(dtoFileToSchemaParserModule.getSchemasWithNames(schemas))
  };
};