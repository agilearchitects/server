import { handlerMethod, Method, } from "../http-handler";
import { IRouteOptions, methodHandler, parseInputHandler, parseOutputHandler, routeHandler, Router, schemas } from "../modules/router.module";

export type routeOptions = Omit<IRouteOptions, "path" | "method">;
export type groupOptions = Omit<IRouteOptions, "path" | "method" | "parseRequest" | "parseResponse">;

export type groupMethod = (path: string, options: groupOptions, routes: Router[]) => Router;
export type verbMethod = (path: string, method: Method, options: routeOptions, ...handlers: handlerMethod[]) => Router;
export type verbsMethod = (path: string, options: routeOptions, ...handlers: handlerMethod[]) => Router;

export type router = {
  verb: verbMethod,
  group: groupMethod,
  get: verbsMethod,
  head: verbsMethod,
  post: verbsMethod,
  put: verbsMethod,
  del: verbsMethod,
  connect: verbsMethod,
  options: verbsMethod,
  trace: verbsMethod,
  patch: verbsMethod,
}

export const routerFactory = (handlerMethods: {
  routeHandler: routeHandler,
  methodHandler: methodHandler,
  parseInputHandler: parseInputHandler,
  parseOutputHandler: parseOutputHandler,
}, schemas: schemas = {}): router => {
  const verb = (path: string , method: Method, options: routeOptions, ...handlers: handlerMethod[]): Router => new Router({ path, method, ...options, schemas: { ...schemas, ...options.schemas } }, { ...handlerMethods }, ...handlers);
  const group = (path: string, options: groupOptions, routes: Router[]): Router => {
    // Creates new route
    const route = new Router({
      path,
      ...options,
      schemas: {
        ...schemas,
        ...options.schemas,
      },
    }, {
      ...handlerMethods,
    }
    );
      // Set each route parent to new route
    for(const childRoute of routes) {
      childRoute.parent = route;
    }
  
    // Add routes to new routes children
    route.children = routes;
  
    return route;
  };
  return {
    verb,
    group,
    get: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.GET, options, ...handlers),
    head: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.HEAD, options, ...handlers),
    post: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.POST, options, ...handlers),
    put: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.PUT, options, ...handlers),
    del: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.DELETE, options, ...handlers),
    connect: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.CONNECT, options, ...handlers),
    options: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.OPTIONS, options, ...handlers),
    trace: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.TRACE, options, ...handlers),
    patch: (path: string, options: routeOptions, ...handlers: handlerMethod[]): Router => verb(path, Method.PATCH, options, ...handlers),
  };

};