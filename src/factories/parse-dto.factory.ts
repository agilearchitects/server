import { jsonType, ObjectToSchemaParserModule, schemasInput } from "../modules/object-to-schema-parser.module";

export type parseDTOMethod = (schemas: schemasInput, object: jsonType, type: string) => jsonType;
export const parseDTOFactory: parseDTOMethod = (schemas: schemasInput, object: jsonType, type: string): jsonType => new ObjectToSchemaParserModule(schemas).parse(object, type);