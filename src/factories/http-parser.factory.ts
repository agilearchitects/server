import {  body, handlerMethod } from "../http-handler";
import { parseInputHttpHandler, parseOutputHttpHandler } from "../http-handler/parser.http-handler";
import { schemasInput } from "../modules/object-to-schema-parser.module";
import { parseDTOMethod } from "./parse-dto.factory";

export const httpParserFactory = (dtoParser: parseDTOMethod): {
  parseInput: <T>(parser: string | ((body: body) => T | Promise<T>), schemas?: schemasInput) => handlerMethod,
  parseOutput: (parser: string | ((body: body) => body | Promise<body>), schemas?: schemasInput) => handlerMethod,
} => ({
  parseInput: <T>(parser: string | ((body: body) => T | Promise<T>), schemas: schemasInput = {}) => parseInputHttpHandler<T>(parser, schemas, dtoParser),
  parseOutput: (parser: string | ((body: body) => body | Promise<body>), schemas: schemasInput = {}) => parseOutputHttpHandler(parser, schemas, dtoParser)
});