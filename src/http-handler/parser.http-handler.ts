import * as stream from "stream";
import { parseDTOMethod } from "../factories/parse-dto.factory";
import { handlerMethod, HttpHandler, next, body, response } from "../http-handler";
import { jsonDict, schemasInput } from "../modules/object-to-schema-parser.module";

export type schemas = schemasInput;

enum ParserError {
  MISSING_PARSER = "missing_parser",
}

export const parseInputHttpHandler = <T>(parser: string | ((body: body) => T | Promise<T>), schemas: schemas = {}, dtoParser?: parseDTOMethod): handlerMethod => async (handler: HttpHandler, next: next) => {
  // Get parsed body
  let body: jsonDict = handler.getPayload<jsonDict>("parsedBody", {});
  if(Object.keys(body).length === 0) {
    if(handler.body instanceof stream.Readable) {
      body = JSON.parse(await streamToString(handler.body));
    } else if(typeof handler.body === "string") {
      body = JSON.parse(handler.body);
    }
  }
  if(typeof parser === "string") {
    if(dtoParser === undefined) {
      throw ParserError.MISSING_PARSER;
    }
    // If parseinput is schema reference. Parse only. Won't update body
    handler.setPayload("parsedBody", dtoParser(schemas, body, parser));
  } else {
    // Update parsedBody to value from custom parseInput response (enables to modify body before reaching handler)
    handler.setPayload("parsedBody", parser(handler.body));
  }
  
  next();
};

export const parseOutputHttpHandler = (parser: string | ((body: body) => body | Promise<body>), schemas: schemas = {}, dtoParser?: parseDTOMethod): handlerMethod => {
  return (handler: HttpHandler, next: next) => {
    handler.beforeSend(async (response: response) => {
      // Will only parse responses with 200 status
      if(response.status !== 200) { return response; }

      if(typeof parser === "string") {
        if(typeof response.body === "string") {
          if(dtoParser === undefined) {
            throw ParserError.MISSING_PARSER;
          }      
          // Run response body through validator
          return {
            ...response,
            body: JSON.stringify(dtoParser(schemas, JSON.parse(response.body), parser)),
          };
        } else {
          throw new Error("Response body is not of type string");
        }
      } else {
        return {
          ...response,
          // Run body through custom validation and return output (enables way of modify body before sending)
          body: await parser(response.body)
        };
      }
    });
    next();
  };
};

const streamToString = (body: stream.Readable): Promise<string> => new Promise((resolve, reject) => {
  let payload: string = "";
  body.on("data", (chunk: unknown) => payload += `${payload}${chunk}`);
  body.on("end", () => resolve(payload));
  body.on("error", (error: unknown) => reject(error));
});