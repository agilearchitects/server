import { handlerMethod, HttpHandler, Method, next } from "../http-handler";

export function corsHttpHandler(origin?: string): handlerMethod;
export function corsHttpHandler(origin: RegExp): handlerMethod;
export function corsHttpHandler(origin: string | RegExp, options: { allowedMethods: Method[], allowedHeaders: string[] }): handlerMethod;
/**
 * Cors handler
 * @param origin What origin to allow
 * @param options What methods and headers to allow (or handler method)
 */
export function corsHttpHandler(origin: string | RegExp = "*", options?: { allowedMethods?: Method[], allowedHeaders?: string[] }): handlerMethod {
  // Default values
  let allowedMethods: Method[] = [Method.GET, Method.PUT, Method.PATCH, Method.POST, Method.DELETE];
  let allowedHeaders: string[] = ["Content-Type", "Authorization"];

  if(options === undefined) {
    options = { };
  }

  // Set default values
  if(options.allowedMethods !== undefined) { allowedMethods = options.allowedMethods; }
  if(options.allowedHeaders !== undefined) { allowedHeaders = options.allowedHeaders; }

  return (handler: HttpHandler, next: next): void => {
    let AccessControlAllowOrigin: string;
    if(origin instanceof RegExp) { // If a regexp of origin is provided
      // Does request have an origin and does it match the provided regexp origin?
      if(handler.origin !== undefined && origin.test(handler.origin) === true) {
        // Set origin to request origin
        AccessControlAllowOrigin = handler.origin;
      } else {
        // No request origin was found or it didn't match. Set the origin (for header response) to string of regexp
        AccessControlAllowOrigin = origin.toString();
      }
    } else {
      // Origin is simple string
      AccessControlAllowOrigin = origin;
    }

    // Set headers for response
    handler.responseHeaders = {
      ...handler.responseHeaders,
      "Access-Control-Allow-Origin": AccessControlAllowOrigin,
      "Vary": "origin"
    };

    if (handler.method === Method.OPTIONS) {
      // Response for cors preflight requests
      handler.send(null, {
        "Access-Control-Allow-Methods": allowedMethods.map((method: Method) => method.toUpperCase()).join(","),
        "Access-Control-Allow-Headers": allowedHeaders.map((header: string) => header.toLowerCase()).join(","),
        "Content-Length": "0",
      }, 204);
    } else {
      // add cors header to response
      next();
    }
  };
}
