import { describe, it } from "@jest/globals";
import { handle, HttpHandler, Method } from "../http-handler";
import { corsHttpHandler } from "./cors.http-handler";

describe("corsHttpHandler", () => {
  it("Should send * as default allowed origin", async () => {
    const response = await handle(Method.GET, "http://127.0.0.1", { }, null,
      corsHttpHandler(),
      (handler: HttpHandler) => { handler.send(null); }
    );
    expect(response.headers["access-control-allow-origin"]).toEqual("*");
  });
  it("Should send matching origin", async () => {
    const origins: string[] = ["http://www.test.test", "http://api.test.test"];
    const response = await handle(Method.GET, "http://127.0.0.1", { origin: origins[0] }, null,
      corsHttpHandler(new RegExp(`(${origins[0]}|${origins[1]})`)),
      (handler: HttpHandler) => { handler.send(null); }
    );
    expect(response.headers["access-control-allow-origin"]).toEqual(origins[0]);
  });
  it("Should send additional headers on option request", async () => {
    const allowedHeaders = ["FOO", "BAR"];
    const allowedMethods = [Method.GET];
    const response = await handle(Method.OPTIONS, "http://127.0.0.1", { }, null,
      corsHttpHandler("*", { allowedHeaders, allowedMethods }),
      (handler: HttpHandler) => { handler.send(null); }
    );
    expect(response.status).toEqual(204);
    expect(response.headers["access-control-allow-methods"]).toEqual(allowedMethods.map((method: Method) => method.toUpperCase()).join(","));
    expect(response.headers["access-control-allow-headers"]).toEqual(allowedHeaders.map((header: string) => header.toLowerCase()).join(","));
  });
});