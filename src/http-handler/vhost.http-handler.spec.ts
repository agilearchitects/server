import { describe, it } from "@jest/globals";
import { handle, HttpHandler, Method } from "../http-handler";
import { vhostHttpHandler } from "./vhost.http-handler";

describe("vhostHttpHandler", () => {
  it("Should validate host using url", async () => {
    const message: string = "hello world";
    const response = await handle(Method.GET, "http://www.test.test", {}, null,
      vhostHttpHandler("www.test.test",
        (handler: HttpHandler) => handler.send(message)
      ),
    );
    expect(response.body).toEqual(message);
  });
  it("Should validate host using header before url", async () => {
    const message: string = "hello world";
    const response = await handle(Method.GET, "http://www.test.test", { host: "api.test.test"}, null,
      vhostHttpHandler("api.test.test",
        (handler: HttpHandler) => handler.send(message)
      ),
    );
    expect(response.body).toEqual(message);
  });
  it("Should skip if host is not matching", async () => {
    const message: string = "hello world";
    const response = await handle(Method.GET, "http://www.test.test", {}, null,
      vhostHttpHandler("api.test.test",
        (handler: HttpHandler) => handler.send(null)
      ),
      (handler: HttpHandler) => handler.send(message)
    );
    expect(response.body).toEqual(message);
  });
  it("Should validate using regexp", async () => {
    const message: string = "hello world";
    const response = await handle(Method.GET, "http://www.test.test", {}, null,
      vhostHttpHandler(/(api\.test\.test|www\.test\.test)/,
        (handler: HttpHandler) => handler.send(message)
      ),
    );
    expect(response.body).toEqual(message);
  });
});