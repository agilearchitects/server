import { describe, it } from "@jest/globals";
import { HttpHandler } from "..";
import { httpParserFactory } from "../factories/http-parser.factory";
import { parseDTOFactory } from "../factories/parse-dto.factory";
import { handle, Method, body } from "../http-handler";
import { schemas } from "./parser.http-handler";

describe("parser", () => {
  const { parseInput, parseOutput } = httpParserFactory(parseDTOFactory);
  it("should be able to parse incoming body", async () => {
    const foo = "bar";
    const body = JSON.stringify({ foo });
    await handle(Method.GET, "http://127.0.0.1", {}, body,
      parseInput((body: body) => {
        const parsedBody = JSON.parse(body as string);
        return { ...parsedBody, foo: (parsedBody.foo as string).toUpperCase() };
      }),
      (handler: HttpHandler) => {
        const body: any = handler.getPayload("parsedBody");
        expect(body.foo).toEqual(foo.toUpperCase());
        return 1; 
      });
  });
  it("should be able to parse outgoing body", async () => {
    const foo = "bar";
    const body = { foo };
    const result = await handle(Method.GET, "http://127.0.0.1", {}, null,
      parseOutput((body: body) => {
        const parsedBody = JSON.parse(body as string);
        return JSON.stringify({ ...parsedBody, foo: (parsedBody.foo as string).toUpperCase() });
      }),
      (handler: HttpHandler) => handler.sendJson(body)
    );
    expect(JSON.parse(result.body as string).foo).toEqual(foo.toUpperCase());
  });
  describe("Schema", () => {
    const schema: schemas = {
      book: {
        properties: {
          id: "number",
          title: "string",
          author: {
            type: "string",
            optional: true,
          },
          store: {
            type: "string | string[]",
            optional: true,
          }
        }
      },
      author: {
        properties: {
          lastname: "string",
          firstname: {
            type: "string",
            optional: true,
          },
          books: {
            type: "book[]",
            optional: true
          }
        }
      }
    };
    it("should parse incoming body as DTO", async () => {
      const author: string = "Jon Doe";
      const body = JSON.stringify({
        id: 1,
        title: "Foo",
        author,
        store: ["Store 1", "Store 2"]
      });
      await handle(Method.GET, "http://127.0.0.1", {}, body, 
        parseInput("book", schema),
        (handler: HttpHandler) => {
          const body: any = handler.getPayload("parsedBody");
          expect(body.author).toEqual(author);
          return 1;
        });
    });
    it("Should be able to parse body nested schema", async () => {
      const lastname = "Jon";
      const body = JSON.stringify({
        lastname,
        books: [
          { id: 1, title: "Foo", }
        ]
      });
      await handle(Method.GET, "http://127.0.0.1", {}, body, 
        parseInput("author", schema),
        (handler: HttpHandler) => {
          const body: any = handler.getPayload("parsedBody");
          expect(body.lastname).toEqual(lastname);
          return 1;
        });
    });
    it("should be able to parse output", async () => {
      const author: string = "Jon Doe";
      const result = await handle(Method.GET, "http://127.0.0.1", {}, null, 
        parseOutput("book", schema),
        () => {
          return JSON.stringify({
            id: 1,
            title: "Foo",
            author,
            store: ["Store 1", "Store 2"]
          });
        });
      const body: any = JSON.parse(result.body as string);
      expect(body.author).toEqual(author);
    });
  });
});