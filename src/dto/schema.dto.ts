import { ISchemaPropertyDTO } from "./schema-property.dto";

export interface ISchemaDTO {
  name: string;
  title?: string;
  description?: string;
  properties: Record<string, ISchemaPropertyDTO>;
}