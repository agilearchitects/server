export interface ISchemaPropertyDTO {
  name: string;
  title?: string;
  type: string;
  description?: string;
  optional?: true;
}