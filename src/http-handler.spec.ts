import * as stream from "stream";
import { describe, it } from "@jest/globals";
import { handle, HttpHandler, Method, response, next } from "./http-handler";

describe("httpHandler", () => {
  it("Should be able to send response object", async () => {
    const body: number = 1;
    const response = await handle(Method.GET, "http://127.0.0.1", {}, "",
      () => { return { body: body, headers: {}, status: 200}; }
    );
    expect(response.body).toEqual(body);
  });
  it("Should be able to send body object", async () => {
    const body: number = 1;
    const response = await handle(Method.GET, "http://127.0.0.1", {}, "",
      () => { return body; },
    );
    expect(response.body).toEqual(body);
  });
  it("Should be able to send response object using send method", async () => {
    const body: number = 1;
    const response = await handle(Method.GET, "http://127.0.0.1", {}, "",
      (handler: HttpHandler) => { handler.send(body); }
    );
    expect(response.body).toEqual(body);
  });
  it("Should be able to send body as stream", async () => {
    const payload: string = "ABCDEFGHOJKLMNOPQRSTUVQ";
    const data: string[] = splitStringEvery(payload, 2);
    const body: stream.PassThrough = new stream.PassThrough();
    const response: response = await handle(Method.GET, "http://127.0.0.1", {}, null, (handler: HttpHandler) => handler.send(body));
    
    if(response.body instanceof stream.Readable) {
      let responseBody = "";
      response.body.on("data", (chunk: unknown) => {
        responseBody += chunk;
      });
      response.body.on("end", () => {
        expect(responseBody).toEqual(payload);
      });
    }
    
    for(const value of data) {
      body.emit("data", value);
      await waitFor(100);
    }
    body.emit("end");
  });
  it("Should be able to handle stream as request body", async () => {
    const payload: string = "ABCDEFGHOJKLMNOPQRSTUVQ";
    const data: string[] = splitStringEvery(payload, 2);
    const body: stream.PassThrough = new stream.PassThrough();

    (async () => {
      await waitFor(100);
      for(const value of data) {
        body.emit("data", value);
        await waitFor(100);
      }
      body.emit("end");
    })();

    const response = await handle(Method.GET, "http://127.0.0.1", {}, body, (handler: HttpHandler) => {
      if(handler.body instanceof stream.Readable) {
        let responseBody = "";
        handler.body.on("data", (chunk: unknown) => {
          responseBody += chunk;
        });
        handler.body.on("end", () => {
          handler.send(responseBody);
        });
      }
    });
    expect(response.body).toEqual(payload);
  });
  describe("beforeSend", () => {
    it("Should be able to modify response before send", async () => {
      const body: string = "hello world";
      const newStatus: number = 404;
      const response: response = await handle(Method.GET, "http://127.0.0.1", {}, "",
        (handler: HttpHandler, next: next) => { handler.beforeSend((response: response) => ({ ...response, status: newStatus })); next(); },
        () => ({ body, headers: {}, status: 200 })
      );
      expect(response.body).toEqual(body);
      expect(response.status).toEqual(newStatus);
    });
  });
});

const splitStringEvery = (value: string, every: number = 5): string[] => {
  const result = value.match(new RegExp(`.{1,${every}}`, "g"));
  return result !== null ? result : [value];
};

const waitFor = (ms: number): Promise<void> => new Promise((resolve) => setTimeout(() => resolve(), ms));