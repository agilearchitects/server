// Libs
import * as http from "http";

// Factories
import { dtoFileToSchemaParserModuleFactory } from "./factories/dto-file-to-schema-parser.factory";
import { fileHandlerFactory, firstFileHandlerFactory, staticContentHandlerFactory } from "./factories/file-handler.factory";
import { httpParserFactory } from "./factories/http-parser.factory";
import { serverFactory, serverFactoryLamdaResponse, serverFactoryNetResponse } from "./factories/http-server.factory";
import { parseDTOFactory } from "./factories/parse-dto.factory";
import { router as routerFactoryReturnType, routerFactory } from "./factories/router.factory";

// Handler
import { handlerMethod } from "./http-handler";

// Http-handlers
import { methodHttpHandler } from "./http-handler/method.http-handler";
import { routeHttpHandler } from "./http-handler/route.http-handler";

// Modules
import { ITs } from "./modules/dto-file-to-schema-parser.module";
import { Router, schemas } from "./modules/router.module";

export const createRouter = (schemas: schemas = {}): routerFactoryReturnType => {
  const { parseInput, parseOutput } = httpParserFactory(parseDTOFactory);
  return routerFactory({
    routeHandler: routeHttpHandler,
    methodHandler: methodHttpHandler,
    parseInputHandler: parseInput,
    parseOutputHandler: parseOutput }, schemas);
};
export const createServer = (...handlers: handlerMethod[]): serverFactoryNetResponse => serverFactory({
  type: "net",
  http,
  handlers
});

export const createLambda = (...handlers: handlerMethod[]): serverFactoryLamdaResponse => serverFactory({ type: "lambda", handlers });

export const routerToSchemas = (dtoRoot: string, router: Router, ts: ITs): schemas => dtoFileToSchemaParserModuleFactory(dtoRoot, ts).parseSchemas(router.allParserSchemaNames);

export const fileHandler = (path: string, headers = {}): handlerMethod => fileHandlerFactory(path, headers);
export const firstFileHandler = (paths: string[], headers = {}): handlerMethod => firstFileHandlerFactory(paths, headers);
export const staticContent = (root: string): handlerMethod => staticContentHandlerFactory(root);