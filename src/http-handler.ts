import * as stream from "stream";
import { Handler, handlerMethod as baseHandlerMethod, handle as baseHandle, next as baseNext, beforeSend } from "./handler";

export enum Method {
  GET = "get",
  HEAD = "head",
  POST = "post",
  PUT = "put",
  DELETE = "delete",
  CONNECT = "connect",
  OPTIONS = "options",
  TRACE = "options",
  PATCH = "patch",
}

export type handlerMethod = baseHandlerMethod<response | body, HttpHandler>;
export type next = baseNext<response | body, HttpHandler>;

export type body = string | number | boolean | null | stream.Readable;
export type header = string | string[];
export type headers = Record<string, header>;
export type query = Record<string, string | string[]>
export type response = { body: body, headers: headers, status: number };
export type request = { method?: Method, url?: string, headers?: headers, body?: body }
export type payload = Record<string, unknown>;

export class HttpHandler extends Handler<response | body> {
  [key: string]: unknown;

  private parsedUrl: URL;

  private _responseHeaders: headers = {};
  public get responseHeaders(): headers {
    return this._responseHeaders;
  }
  public set responseHeaders(headers: headers) {
    this._responseHeaders = this.headerKeysToLowercase(headers);
  }

  private readonly defaultUrl: string = "http://127.0.0.1";
  private readonly defaultPorts: Record<string, number> = { http: 80, https: 443 };

  private _method: Method;
  private _url: string;
  private _headers: headers = {};
  private _body: body = null;
  public constructor(
    request: request,
  ) {
    super();
    this._method = request !== undefined && request.method !== undefined ? request.method : Method.GET;
    this._url = request !== undefined && request.url !== undefined ? request.url : this.defaultUrl;
    this._headers = request !== undefined && request.headers !== undefined ? request.headers : {};
    this._body = request !== undefined && request.body !== undefined ? request.body : null;
    
    this.parsedUrl = new URL(this.url);
  }

  public get method(): Method | undefined { return this._method; }
  public get url(): string { return this._url; }
  public get body(): body { return this._body; }
  public get headers(): headers { return this._headers; }
  public get protocol(): string { return this.parsedUrl.protocol.replace(/:$/, ""); }

  public get port(): number | undefined {
    if(this.parsedUrl === undefined) { return undefined; }
    if(this.parsedUrl.port !== "") {
      return parseInt(this.parsedUrl.port, 10);
    } else if (this.protocol in this.defaultPorts) {
      return this.defaultPorts[this.protocol];
    }
    return NaN;
  }

  public get path(): string {
    return this.parsedUrl.pathname;
  }

  public get host(): string {
    return typeof this.headers.host === "string" ? this.headers.host.replace(/:\d+$/, "").toLowerCase() : this.parsedHost; 
  }

  public get parsedHost(): string {
    return this.parsedUrl.hostname;
  }

  public get origin(): string | undefined {
    if("origin" in this.headers && typeof this.headers["origin"] === "string") {
      return this.headers["origin"];
    }
    return undefined;
  }

  public get query(): query {
    if (this.parsedUrl.search === "") { return {}; }
    const query: query = {};
    this.parsedUrl.searchParams.forEach((value: string, key: string) => {
      const foundValue: string | string[] | undefined = query[key];

      query[key] = foundValue === undefined ? value : [...(typeof foundValue === "string" ? [foundValue] : foundValue), value];
    });

    return query;
  }

  public beforeSend(callback: beforeSend<response>): void {
    super.beforeSend((response: response | body) => {
      if(typeof response === "object" && response !== null && "body" in response) { return callback(response); }
      else { throw new Error(); }
    });
  }

  public send(body: body): void;
  public send(body: body, headers?: headers, status?: number): void;
  public send(response: response): void;
  public send(response: response | body, headers: headers = {}, status: number = 200): void {
    if(typeof response === "object" && response !== null && "body" in response) {
      response = { ...response, headers: { ...this.responseHeaders, ...this.headerKeysToLowercase(response.headers), } };
    } else {
      response = { body: response, headers: { ...this.responseHeaders, ...this.headerKeysToLowercase(headers) }, status };
    }
    super.send(response);
  }

  public sendStatus(status: number): void {
    this.send(null, {}, status);
  }

  public sendJson(object: unknown): void;
  public sendJson(object: unknown, headers?: headers, status?: number): void {
    this.send(JSON.stringify(object), headers, status);
  }

  public getPayload<T>(name: string): T | undefined;
  public getPayload<T>(name: string, defaultValue: T): T;
  public getPayload<T>(name: string, defaultValue?: T): T | undefined { return name in this ? this[name] as T : defaultValue !== undefined ? defaultValue : undefined; }

  public setPayload(name: string, value: unknown): void;
  public setPayload(payload: payload): void;
  public setPayload(payload: string | payload, value?: unknown): void {
    if (typeof payload === "string") {
      this[payload] = value;
    } else {
      for(const key of Object.keys(payload)) {
        this[key] = payload[key];
      }
    }
  }

  private headerKeysToLowercase(headers: headers): headers {
    return Object.keys(headers).reduce((previousValue: headers, key: string) => {
      const value = headers[key];
      const foundValue: string | string[] | undefined = previousValue[key.toLowerCase()];
      return {
        ...previousValue,
        // Save header key as lower case
        [key.toLowerCase()]: foundValue === undefined ? value : [
          ...(typeof foundValue === "string" ? [foundValue] : foundValue),
          ...(typeof value === "string" ? [value] : value)],
      };
    }, {});
  }
}

export const handle = async (method: Method, url: string, headers: headers, body: body, ...handlers: handlerMethod[]): Promise<response> => {
  const handler = new HttpHandler({ method, url, headers, body });
  const response: response | body = await baseHandle(handler, ...handlers);

  if(typeof response === "object" && response !== null && "body" in response) {
    return response;
  } else {
    return { body: response, headers: {}, status: 200 };
  }
};