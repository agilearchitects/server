export type beforeSend<RESPONSE> = (response: RESPONSE) => RESPONSE | Promise<RESPONSE>;
export type onSend<RESPONSE> = (response: RESPONSE) => void;
export type onError = (error: unknown) => void;

export enum handlerError {
  ALREADY_SENT = "already_sent",
  NO_RESPONSE = "no_response",
}

export type handlerMethod<T = unknown, R extends Handler<T> = Handler<T>> = (handler: R, next: next<T, R>) => T | void | Promise<T | void>;
export type next<T = unknown, R extends Handler<T> = Handler<T>> = (...handlers: handlerMethod<T, R>[]) => void;

export class Handler<T = unknown> {
  private _hasSent: boolean = false;
  public get hasSent(): boolean { return this._hasSent; }

  private beforeSends: beforeSend<T>[] = [];
  private onSends: onSend<T>[] = [];
  private onErrors: onError[] = [];
  
  public send(response: T): void {
    (async () => {
      // Will not emit send if already sent
      if(this.hasSent === true) { throw handlerError.ALREADY_SENT; }

      // Execute all beforeSend callbacks one by one and update response with every itteration
      for(const beforeSend of this.beforeSends) {
        response = await beforeSend(response);
      }
      // Return response
      return response;
    })().then((response: T) => {
      // Set hasSent to true (prevents sending more than once)
      this._hasSent = true;

      // Emit all onSend callbacks
      for(const onSend of this.onSends) {
        onSend(response);
      }
    }).catch((error: unknown) => {
      // Emit all onError callbacks
      for(const onError of this.onErrors) {
        onError(error);
      }
    });
  }

  public beforeSend(callback: beforeSend<T>): void {
    this.beforeSends = [...this.beforeSends, callback];
  }
  public onSend(callback: onSend<T>): void {
    this.onSends = [...this.onSends, callback];
  }
  public onError(callback: onError): void {
    this.onErrors = [...this.onErrors, callback];
  }
}

export const handle = <T = unknown, R extends Handler<T> = Handler<T>>(handler: R, ...handlers: handlerMethod<T, R>[]): Promise<T> => {
  return new Promise((resolve, reject) => {
    // Resolve on send
    handler.onSend((response: T) => resolve(response));
    handler.onError((error: unknown) => reject(error));
    
    let waitToResolve: boolean = false;
    (async () => {
      if(handlers.length === 0) { throw handlerError.NO_RESPONSE; }

      return await handlers[0](handler, (...nextHandlers: handlerMethod<T, R>[]) => {
        // If a next statement is called
        waitToResolve = true;
        handle(handler, ...nextHandlers, ...handlers.slice(1))
          // Send response if resolved
          .then((response: T) => handler.send(response))
          .catch((error: unknown) => reject(error));
      });
    })().then((response: T | void) => {
      if(waitToResolve === true) { return; }
      if(response !== undefined) {
        handler.send(response);
      }
      /* !!!WARNING!!! If response is undefined the handler will never complete
      Example of this is handler methods ending without returning anything and
      not calling handler.send() If listening to undefined responses the handler
      would finish before if any handler method calls handle.send async */
    }).catch((error: unknown) => reject(error));
  });
};

export const basicHandle = <T = unknown>(...handlers: handlerMethod<T, Handler<T>>[]): Promise<T> => handle(new Handler<T>(), ...handlers);