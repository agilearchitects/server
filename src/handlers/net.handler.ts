import * as net from "net";
import * as stream from "stream";
import { handlerError } from "../handler";
import { response } from "../http-handler";
import { handle, headers, Method, handlerMethod } from "../http-handler";

interface IIncomingMessage<SOCKET extends net.Socket = net.Socket> extends stream.Readable {
  socket: SOCKET;
  headers: Record<string, string | string[] | undefined>;
  url?: string;
  method?: string;
}

interface IServerResponse extends stream.Writable {
  headersSent: boolean;
  statusCode: number;
  setHeader: (key: string, value: string | string[]) => void;
  end: (chunk: unknown) => this;
}

interface IServer {
  listen: (port: number, callback?: () => void) => this;
  close: (callback?: (err?: Error) => void) => this;
}

export interface IHttp {
  createServer: (requestListener?: (req: IIncomingMessage, res: IServerResponse) => void) => IServer;
}

interface netHandlerOptions {
  http: IHttp;
  onError?: (error: unknown) => void;
}

export function netHandler(http: IHttp, ...handlers: handlerMethod[]): IServer;
export function netHandler(options: netHandlerOptions, ...handlers: handlerMethod[]): IServer;
export function netHandler(options: IHttp | netHandlerOptions, ...handlers: handlerMethod[]): IServer {
  if(!("http" in options)) {
    options = { http: options };
  }
  return options.http.createServer(async (request: IIncomingMessage, response: IServerResponse) => {
    const url: string = `${"encrypted" in request.socket ? "http" : "https"}://${request.headers.host}${request.url}`;
    const requestMethod = request.method !== undefined ? request.method.toUpperCase() : "GET";
    const method: Method = requestMethod in Method ? Method[requestMethod as keyof typeof Method] : Method.GET;
    // Map headers
    const headers = Object.keys(request.headers).reduce((previousValue: headers, key: string) => {
      return {
        ...previousValue,
        ...((value?: string | string[]) => (value !== undefined ? { [key]: value } : undefined))(request.headers[key]),
      };
    }, {});

    let handleResponse: response;
    try {
      handleResponse = await handle(method, url, headers, request, ...handlers);
    } catch(error: unknown) {
      if("onError" in options && options.onError !== undefined) {
        options.onError(error);
      } else {
        console.error(error);
      }
      
      const status: number = error === handlerError.NO_RESPONSE ? 404 : 500;

      handleResponse = { body: null, status, headers: {} };
    }
  
    // Set http status code
    response.statusCode = handleResponse.status;
    // Set headers
    for (const key of Object.keys(handleResponse.headers)) {
      response.setHeader(key, handleResponse.headers[key]);
    }
    // If body is a readable stream. add http stream as pipe
    if (handleResponse.body instanceof stream.Readable) {
      handleResponse.body.pipe(response);
    } else {
    // Body is an object. Send whole body to response
      response.end(handleResponse.body);
    }
  });
}