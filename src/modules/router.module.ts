import { handlerMethod, next, HttpHandler, Method, body } from "../http-handler";
import { schemas as parserSchemas } from "../http-handler/parser.http-handler";
import { arrayTypeAsType, isSchemaType, splitTypes } from "./schema.module";

export type schemas = parserSchemas;
export type routeHandler = (route: string | RegExp, ...handlers: handlerMethod[]) => handlerMethod;
export type methodHandler = (method: Method, ...handlers: handlerMethod[]) => handlerMethod;
export type  parseInputHandler = <T>(parser: string | ((body: body) => T | Promise<T>), schemas: schemas) => handlerMethod;
export type parseOutputHandler = (parser: string | ((body: body) => body | Promise<body>), schemas: schemas) => handlerMethod;

export type middleware = (handlerMethod | IMiddleware);
interface IMiddleware {
  name?: string;
  description?: string;
  handler: handlerMethod;
}

export interface IRouteOptions {
  path: string;
  method?: Method;
  name?: string;
  description?: string;
  parseRequest?: string;
  parseResponse?: string;
  middlewares?: middleware[];
  schemas?: schemas;
}

export class Router {
  public parent?: Router;
  public children?: Router[];
  public schemas: schemas = {};
  
  private routeHandler: routeHandler;
  private methodHandler: methodHandler;
  private parseInputHandler: parseInputHandler;
  private parseOutputHandler: parseOutputHandler;

  private _path: string = "";
  public get path(): string { return this._path; }
  private _method: Method | undefined;
  public get method(): Method | undefined { return this._method; }
  private _name: string | undefined;
  public get name(): string | undefined { return this._name; }
  private _description: string | undefined;
  public get description(): string | undefined { return this._description; }
  private _parseRequest: string | undefined;
  public get parseRequest(): string | undefined { return this._parseRequest; }
  private _parseResponse: string | undefined;
  public get parseResponse(): string | undefined { return this._parseResponse; }
  private _middlewares: IMiddleware[] | undefined;
  public get middlewares(): IMiddleware[] | undefined { return this._middlewares; }
  private _handlers: handlerMethod[];
  public get handlers(): handlerMethod[] { return this._handlers; }
  
  public constructor(
    options: IRouteOptions,
    factory: {
      routeHandler: routeHandler,
      methodHandler: methodHandler,
      parseInputHandler: parseInputHandler,
      parseOutputHandler: parseOutputHandler,
    },
    ...handlers: handlerMethod[]
  ) {
    this._path = options.path;
    if(options.method !== undefined) {
      this._method = options.method;
    }
    if(options.name !== undefined) {
      this._name = options.name;
    }
    if(options.description !== undefined) {
      this._description = options.description;
    }
    if(options.parseRequest !== undefined) {
      this._parseRequest = options.parseRequest;
    }
    if(options.parseResponse !== undefined) {
      this._parseResponse = options.parseResponse;
    }
    if(options.middlewares !== undefined) {
      this._middlewares = this.mapMiddlewares(options.middlewares);
    }
    if(options.schemas !== undefined) {
      this.schemas = options.schemas;
    }

    this.routeHandler = factory.routeHandler;
    this.methodHandler = factory.methodHandler;
    this.parseInputHandler = factory.parseInputHandler;
    this.parseOutputHandler = factory.parseOutputHandler;

    this._handlers = handlers;
  }

  public get fullPath(): string {
    // Full path is the routes current path prefixed by parent path (if not itself prefixed by slash)
    if(/^\//.test(this.path) === false && this.parent !== undefined) {
      return `${this.parent.fullPath}${this.parent.fullPath !== "/" && this.path !== "" ? "/" : ""}${this.path}`;
    }
    return `/${this.path}`;
  }

  public get allMiddlewares(): IMiddleware[] {
    return [
      ...(this.parent !== undefined ? this.parent.allMiddlewares : []),
      ...(this.middlewares !== undefined ? this.middlewares : [])
    ];
  }

  public get allSchemas(): schemas {
    return {
      ...(this.parent !== undefined ? this.parent.allSchemas : undefined),
      ...this.schemas,
    };
  }

  public get parserSchemaNames(): string[] {
    return [
      ...this.parseRequest !== undefined ? splitTypes(this.parseRequest).map((type: string) => arrayTypeAsType(type)).filter((type: string) => isSchemaType(type)) : [],
      ...this.parseResponse !== undefined ? splitTypes(this.parseResponse).map((type: string) => arrayTypeAsType(type)).filter((type: string) => isSchemaType(type)) : []
    ];
  }
  public get allParserSchemaNames(): string[] {
    return [
      ...(this.children !== undefined ? this.children.reduce((previousValue: string[], child: Router) => [...previousValue, ...child.allParserSchemaNames], []) : []),
      ...this.parserSchemaNames
    ];
  }

  public handler: handlerMethod = (handler: HttpHandler, next: next) => {
    if(this.children !== undefined) {
      next(...this.children.map((child: Router) => child.handler));
      return;
    }

    const pathHandlers = this.routeHandler(this.fullPath, ...this.mapMiddlewaresToHandlers(this.allMiddlewares), ...[
      ...(this.parseRequest !== undefined ? [this.parseInputHandler(this.parseRequest, this.allSchemas)] : []),
      ...(this.parseResponse !== undefined ? [this.parseOutputHandler(this.parseResponse, this.allSchemas)] : []),
      ...this.handlers,
    ]);
    if(this.method !== undefined) { 
      next(this.methodHandler(this.method, pathHandlers));
    } else {
      next(pathHandlers);
    }
  };

  private mapMiddlewares(middlewares: middleware[]): IMiddleware[] {
    return middlewares.map((middleware: middleware) => "handler" in middleware ? middleware : { handler: middleware });
  }

  private mapMiddlewaresToHandlers(middlewares: IMiddleware[]): handlerMethod[] {
    return middlewares.map((middleware: IMiddleware) => middleware.handler);
  }
}