import ts, { SyntaxKind, CompilerOptions, CompilerHost, Program, Diagnostic } from "typescript";
import { ISchemaPropertyDTO } from "../dto/schema-property.dto";
import { ISchemaDTO } from "../dto/schema.dto";
import { schemaPropertiesInput, schemasInput } from "./object-to-schema-parser.module";
import { SchemaModule, schemaProperties } from "./schema.module";

export interface ITs {
  createProgram(rootNames: readonly string[], options: CompilerOptions, host?: CompilerHost, oldProgram?: Program, configFileParsingDiagnostics?: readonly Diagnostic[]): Program;
  SyntaxKind: typeof SyntaxKind;
}

export enum dtoParserError {
  FILE_NOT_FOUND = "file_not_found",
  INTERFACE_NOT_FOUND = "interface_not_found",
}

export class DTOFileToSchemaParserModule extends SchemaModule {
  public constructor(
    private readonly dtoRoot: string,
    private readonly tsModule: ITs,
  ) { super(); }

  public getSchemasWithNames(schemaNames: string[]): ISchemaDTO[] {
    // Container for schemas to return
    let schemas: ISchemaDTO[] = [];

    // Iterate of schema names
    for(let a: number = 0; a < schemaNames.length; a++) {
      // Get schema
      const schema: ISchemaDTO = this.parseFile(schemaNames[a]);
      
      // Update schemaNames with schemaNames from schema properties (exclude existing schemas)
      schemaNames = [
        ...schemaNames,
        ...this.getSchemaNamesFromProperties(schema).filter((schema: string) => schemaNames.includes(schema) === false)
      ];

      // Add schemas to return container
      schemas = [
        ...schemas,
        schema,
      ];
    }

    // Return schemas as dictionary
    return schemas;
  }

  public schemasToSchemaInput(schemas: ISchemaDTO[]): schemasInput {
    return schemas.reduce((previousValue: schemasInput, schema: ISchemaDTO) => ({
      ...previousValue,
      [schema.name]: {
        ...(schema.title !== undefined ? { title: schema.title } : undefined),
        ...(schema.description !== undefined ? { description: schema.description } : undefined),
        properties: Object.keys(schema.properties).reduce((previousValue: schemaPropertiesInput, key: string) => {
          const property = schema.properties[key];
          return {
            ...previousValue,
            [key]: this.propertyIsSimple(property) === true ? property.type : {
              type: property.type,
              ...(property.title !== undefined ? { title: property.title } : undefined),
              ...(property.description !== undefined ? { description: property.description } : undefined),
              ...(property.optional !== undefined ? { optional: property.optional } : undefined),
            }
          };
        }, {})
      }
    }), {});
  }

  public parseFile(schemaName: string): ISchemaDTO {
    const path: string = this.schemaNameToPath(schemaName);
    const program = this.tsModule.createProgram([path], {});
    const file = program.getSourceFile(path);
    if (file === undefined) {
      throw dtoParserError.FILE_NOT_FOUND;
    }
    
    const myInterface: ts.InterfaceDeclaration | undefined = this.getInterfaceFromFile(file, this.schemaNameToInterfaceName(schemaName));
  
    if (myInterface === undefined) {
      throw dtoParserError.INTERFACE_NOT_FOUND;
    }
  
    const name: string = this.interfaceNameToSchemaName(myInterface.name.escapedText.toString());
    const description: string | undefined = this.getCommentBeforeNode(file, myInterface);
    return {
      name,
      ...(description !== undefined ? { description } : undefined),
      properties: myInterface.members.reduce((previousValue: schemaProperties, member: ts.TypeElement) => {
        const property: ISchemaPropertyDTO = this.typeElementToProperty(file, program, member);
        return {
          ...previousValue,
          [property.name]: {
            ...property,
          }
        };
      }, {}),
    };
  }

  private propertyIsSimple(property: ISchemaPropertyDTO): boolean {
    return property.description === undefined && property.optional === undefined && property.title === undefined;
  }

  private getInterfaceFromFile(file: ts.SourceFile, interfaceName: string): ts.InterfaceDeclaration | undefined {
    const firstNode: ts.Node = file.getChildAt(0);
    const foundInterface: ts.InterfaceDeclaration | undefined = firstNode.getChildren().find((child: ts.Node) => {
      // Determine type
      if(child.kind !== this.tsModule.SyntaxKind.InterfaceDeclaration) {
        return false;
      }

      // Determine name exists
      if((child as any).name === undefined || ((child as any).name !== undefined && !("escapedText" in (child as any).name))) {
        return false;
      }
      
      // Determine name match
      if((child as any).name.escapedText !== interfaceName) {
        return false;
      }

      return true;
    }) as ts.InterfaceDeclaration | undefined;

    return foundInterface;
  }

  private getCommentBeforeNode(file: ts.SourceFile, node: ts.Node): string | undefined {
    // Match either //comment or /*comment*/
    const match = file.text.substring(node.pos, node.end).match(/^\s*(?:\/\/([^\n]*)|\/\*((?:(?!\*\/).)*))/s);
    if (match !== null) {
      if (match[1] !== undefined) {
        return match[1].trim();
      }
      else {
        return match[2].trim();
      }
    }
  }

  private typeElementToProperty(file: ts.SourceFile, program: ts.Program, element: ts.TypeElement): ISchemaPropertyDTO {
    const typeChecker = program.getTypeChecker();
    const match = file.text.substring(element.pos, element.end).match(/(?:\/\/([^\n]*)|\/\*((?:(?!\*\/).)*))/s);

    if(element.name === undefined || (element.name !== undefined && !("escapedText" in element.name))) {
      throw dtoParserError.INTERFACE_NOT_FOUND;
    }

    let description: string | undefined;
    if (match !== null) {
      if (match[1] !== undefined) { description = match[1].trim(); }
      else { description = match[2].trim(); }
    }
    return {
      name: element.name.escapedText.toString(),
      ...(description !== undefined ? { description } : {}),
      type: this.typeToTypeWithSchemaNames(typeChecker.typeToString(typeChecker.getTypeAtLocation(element))),
      ...(element.questionToken !== undefined ? { optional: true } : {}),
    };
  }

  private typeToTypeWithSchemaNames(type: string): string {
    return this.joinTypes(this.splitTypes(type).map((type: string) => {
      if(this.isSimpleType(this.arrayTypeAsType(type))) {
        return type;
      }
      return `${this.interfaceNameToSchemaName(this.arrayTypeAsType(type))}${this.isArrayType(type) ? "[]" : ""}`;
    }));
  }
  private getSchemasFromType(type: string): string[] {
    return this.splitTypes(type).reduce((previousValue: string[], type: string) => {
      // Convert to type without array suffix (if any)
      type = this.arrayTypeAsType(type);
      return [
        ...previousValue,
        ...(this.isSimpleType(type) === false ? [type] : [])
      ];
    }, []);
  }
  private getSchemaNamesFromProperties(schema: ISchemaDTO): string[] {
    return Object.keys(schema.properties)
      .map((key: string) => schema.properties[key].type)
      .reduce((previousValue: string[], type: string) => [
        ...previousValue,
        ...this.getSchemasFromType(type)
      ], []);
  }
  private schemaNameToPath(schemaName: string): string {
    return `${this.dtoRoot}/${this.toKebabCase(schemaName)}.dto.ts`;
  }
}