import { ISchemaPropertyDTO } from "../dto/schema-property.dto";
import { ISchemaDTO } from "../dto/schema.dto";

export type schemas = Record<string, ISchemaDTO>;
export type schemaProperties = Record<string, ISchemaPropertyDTO>;

export class SchemaModule {
  protected interfaceNameToSchemaName(interfaceName: string): string {
    return this.toCamelCase(interfaceName.substring(1, interfaceName.length - 3));
  }
  protected schemaNameToInterfaceName(schemaName: string):string {
    return `I${this.toPascalCase(schemaName)}DTO`;
  }
  
  protected toKebabCase(value: string): string {
    return `${value.replace(/[A-Z]/g, (match: string, offset: number) => `${offset !== 0 ? "-" : ""}${match.toLowerCase()}`)}`;
  }
  protected toPascalCase(value: string): string {
    return `${value.slice(0,1).toUpperCase()}${this.toCamelCase(value.slice(1))}`;
  }
  protected toCamelCase(value: string): string {
    return value.replace(/(-|_)\w/g, (match: string) => match.slice(1,2).toUpperCase()).replace(/^\w/, (match: string) => match.toLowerCase());
  }

  protected splitTypes(type: string): string[] {
    return splitTypes(type);
  }

  protected joinTypes(types: string[]): string {
    return types.join(" | ");
  }

  protected isMultiType(type: string): boolean {
    return isMultiType(type);
  }
  
  protected isArrayType(type: string): boolean {
    return isArrayType(type);
  }

  protected isSimpleType(type: string): boolean {
    return isSimpleType(type);
  }

  protected isSchemaType(type: string): boolean {
    return isSchemaType(type);
  }

  protected arrayTypeAsType(type: string): string {
    return arrayTypeAsType(type);
  }
}
export const splitTypes = (type: string): string[] => type.split("|").map((type: string) => type.trim());
export const isMultiType = (type: string): boolean => /\|/.test(type);
export const isArrayType = (type: string): boolean => (/.*\[\]$/).test(type);
export const isSimpleType = (type: string): boolean => ["string", "number", "boolean", "null"].includes(type);
export const isSchemaType = (type: string): boolean => isSimpleType(type) === false;
export const arrayTypeAsType = (type: string): string => type.replace(/\[\]$/, "");
