import { ISchemaPropertyDTO } from "../dto/schema-property.dto";
import { ISchemaDTO } from "../dto/schema.dto";
import { SchemaModule, schemaProperties, schemas } from "./schema.module";

export type schemaPropertiesInput = Record<string, Omit<ISchemaPropertyDTO, "name"> | string>;
interface ISchemaInput extends Omit<ISchemaDTO, "name" | "properties"> {
  properties: schemaPropertiesInput;
}
export type schemasInput = Record<string, ISchemaInput>;

export enum parseError {
  SCHEMA_NOT_FOUND = "schema_not_found",
  PROPERTY_VALUE_NOT_FOUND = "property_value_not_found",
  PROPERTY_NOT_EXISTS_IN_SCHEMA = "property_not_exists_in_schema",
  WRONG_TYPE = "wrong_type",
  UNEXPECTED_ERROR = "unexpected_error",
}

export class ParseError extends Error {
  private _schema?: ISchemaDTO;
  public get schema(): ISchemaDTO | undefined { return this._schema; }
  private _property?: ISchemaPropertyDTO;
  public get property(): ISchemaPropertyDTO | undefined { return this._property; }
  
  public constructor(
    public readonly code: parseError,
    options: { schema?: ISchemaDTO, property?: ISchemaPropertyDTO },
    message?: string,
  ) {
    super(message);
    const { schema, property } = options;
    if(schema !== undefined) { this._schema = schema; }
    if(property !== undefined) { this._property = property; }
  }
}

export type simpleType = string | number | boolean | null;
export type jsonDict = { [key: string]: jsonType };
export type jsonType = simpleType | jsonDict | jsonType[];

export class ObjectToSchemaParserModule extends SchemaModule {
  private schemas: schemas;

  public constructor(
    schemas: schemasInput = {},
  ) {
    super();
    this.schemas = this.mapSchemasInputToSchemas(schemas);
  }

  public parse(object: jsonType, type: string): jsonType {
    // Expect multi type
    if(this.isMultiType(type)) {
      // parse each type (return first that's successful)
      for(const singleType of this.splitTypes(type)) {
        try {
          return this.parse(object, singleType);
        } catch { /** */ }
      }

      // Throw if all type casting fails
      throw new ParseError(parseError.WRONG_TYPE, {}, `Expected ${type}, got ${typeof object}`);
    }

    // Expect array
    if(this.isArrayType(type)) {
      if(!(object instanceof Array)) {
        throw new ParseError(parseError.WRONG_TYPE, {}, `Expected object to be instance of array byt got ${typeof object}`);
      }
      // Parse each separate object with parseType
      return object.map(object => this.parse(object, this.arrayTypeAsType(type)));
    }

    // Expect simple type
    if(this.isSimpleType(type)) {
      if(this.isObjectSimpleType(object) === false) {
        throw new ParseError(parseError.WRONG_TYPE, {}, `Expected object to be simple object "${type}" but got complex`);
      }
      if(this.simpleObjectMatchesSimpleType(object as simpleType, type) === false) {
        throw new ParseError(parseError.WRONG_TYPE, {}, `Expected object to be "${type}", got "${typeof object}"`);
      }
      return object;
    }

    // Expect shcema type
    if(this.isSchemaType(type)) {
      const schemaName = this.toCamelCase(type);

      if(this.isObjectDict(object) === false) {
        throw new ParseError(parseError.WRONG_TYPE, {}, `Expected object to be "${schemaName}", got "${typeof object}"`);
      }
      if(this.schemaExists(schemaName) === false) {
        throw new ParseError(parseError.SCHEMA_NOT_FOUND, {}, `Schema with name ${schemaName} could not be found`);
      }
      return this.parseSchema(object as jsonDict, this.schemas[schemaName]);
    }

    throw new ParseError(parseError.UNEXPECTED_ERROR, {});
  }

  private parseSchema(object: jsonDict, schema: ISchemaDTO): jsonDict {
    const { properties } = schema;
    
    // Validate that all props in object exist as definitions in schema
    for(const key of Object.keys(object)) {
      if(this.keyExistsInSchema(key, schema) === false) {
        throw new ParseError(parseError.PROPERTY_NOT_EXISTS_IN_SCHEMA, { schema }, `Property ${key} in object is missing in schema`);
      }
    }

    // Parse each property
    return Object.keys(properties).reduce((previousValue: jsonDict, key: string) => {
      const property: ISchemaPropertyDTO = properties[key];
      const value: jsonType = object[key];
      
      if(this.isPropertyRequired(property) === true && this.propertyExistsInObject(object, key) === false) {
        throw new ParseError(parseError.PROPERTY_VALUE_NOT_FOUND, { property, schema }, `Property ${property.name} not found`);
      }

      return {
        ...previousValue,
        ...(value !== undefined ? { [key]: this.parse(value, property.type) } : undefined)
      };
    }, {});
  }

  private propertyExistsInObject(object: Record<string, unknown>, key: string): boolean {
    return key in object;
  }
  
  private keyExistsInSchema(key: string, schema: ISchemaDTO): boolean {
    return schema.properties[key] !== undefined;
  }
  
  private isObjectSimpleType(object: jsonType): boolean {
    return typeof object === "string" || typeof object === "number" || typeof object === "boolean" || object === null;
  }
  
  private isObjectDict(object: jsonType): boolean {
    return typeof object !== "string" && typeof object !== "number" && typeof object !== "boolean" && object !== null && !(object instanceof Array);
  }


  private isPropertyRequired(property: ISchemaPropertyDTO): boolean {
    return property.optional === undefined;
  }

  private simpleObjectMatchesSimpleType(object: simpleType, type: string): boolean {
    return (type === "null" && object === null) || typeof object === type;
  }

  private schemaExists(schemaName: string): boolean {
    return this.schemas[schemaName] !== undefined;
  }

  private mapSchemasInputToSchemas(schemas: schemasInput): schemas {
    return Object.keys(schemas).reduce((previousValue: schemas, key: string) => ({
      ...previousValue,
      [key]: {
        name: key,
        ...schemas[key],
        properties: this.mapSchemaPropertiesInputToSchemaProperties(schemas[key].properties)
      }
    }), {});
  }
  private mapSchemaPropertiesInputToSchemaProperties(properties: schemaPropertiesInput): schemaProperties {
    return Object.keys(properties).reduce((previousValue: schemaProperties, key: string) => {
      const property = properties[key];
      return {
        ...previousValue,
        [key]: {
          name: key,
          ...(typeof property === "string" ? { type: property } : property),
        }
      };
    }, {});
  }
}
