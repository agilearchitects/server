import { describe, it } from "@jest/globals";
import { ObjectToSchemaParserModule, parseError, ParseError, schemasInput } from "./object-to-schema-parser.module";

describe("ParseModule", () => {
  describe("Multi type", () => {
    it("should parse", () => {
      const values = ["foo", 1, [true, false]];

      const dtoParser = new ObjectToSchemaParserModule();
      const results = values.map(value => dtoParser.parse(value, "string | number | boolean[]"));
      
      for(let a = 0; a < results.length; a++) {
        expect(results[a]).toEqual(values[a]);
      }
    });
    it("should throw error", () => {
      let coughtError: unknown;
      try {
        new ObjectToSchemaParserModule().parse("foo", "boolean");
      } catch(error: unknown) {
        coughtError = error;
      }
      
      expect(coughtError).toBeDefined();
      expect(coughtError).toBeInstanceOf(ParseError);
      expect((coughtError as ParseError).code).toEqual(parseError.WRONG_TYPE);
    });
  });
  describe("Array type", () => {
    it("should parse", () => {
      const values = [{
        value: ["foo", "bar"],
        type: "string[]"
      }, {
        value: [1, 2],
        type: "number[]"
      }, {
        value: [true, false],
        type: "boolean[]"
      }, {
        value: [null, null],
        type: "null[]"
      }];

      const dtoParser = new ObjectToSchemaParserModule();
      const results = values.map(value => dtoParser.parse(value.value, value.type));
    
      for(let a = 0; a < results.length; a++) {
        expect(results[a]).toEqual(values[a].value);
        expect(results[a]).toBeInstanceOf(Array);
        expect(results[a]).toHaveLength(2);
      }
    });
    it("should throw error", () => {
      try {
        new ObjectToSchemaParserModule().parse("foo", "string[]");
      } catch(error: unknown) {
        expect(error).toBeInstanceOf(ParseError);
        expect((error as ParseError).code).toEqual(parseError.WRONG_TYPE);
      }
    });
  });
  describe("Simple type", () => {
    it("should be able to parse", () => {
      const values = [{
        value: "foo",
        type: "string"
      }, {
        value: 1,
        type: "number"
      }, {
        value: true,
        type: "boolean"
      }, {
        value: null,
        type: "null"
      }];

      const dtoParser = new ObjectToSchemaParserModule();
      const results = values.map(value => dtoParser.parse(value.value, value.type));
    
      for(let a = 0; a < results.length; a++) {
        expect(results[a]).toEqual(values[a].value);
      }
    });
    it("should throw error", () => {
      try {
        new ObjectToSchemaParserModule().parse("foo", "number");
      } catch(error: unknown) {
        expect(error).toBeInstanceOf(ParseError);
        expect((error as ParseError).code).toEqual(parseError.WRONG_TYPE);
      }
    });
  });
  describe("Schema type", () => {
    const schemas: schemasInput = {
      book: {
        properties: {
          title: "string",
          author: "string",
          store: { type: "string | store | store[]", optional: true }
        }
      },
      store: {
        properties: {
          name: "string",
          street: { type: "string", optional: true }
        }
      }
    };
    it("should parse", () => {
      const value = { title: "my book", author: "Jon Doe", store: "The store" };
      new ObjectToSchemaParserModule(schemas).parse(value, "book");
    });
    it("should parse without requiring optional params", () => {
      const value = { title: "my book", author: "Jon Doe" };
      new ObjectToSchemaParserModule(schemas).parse(value, "book");
    });
    it("should parse nested schemas", () => {
      const value = [
        { title: "my book", author: "Jon Doe", store: { name: "Superstore USA", street: "First Street 1" } },
        { title: "my book", author: "Jon Doe", store: [{ name: "Superstore USA" }, { name: "Dealbreaker" }] },
      ];
      new ObjectToSchemaParserModule(schemas).parse(value, "book[]");
    });
    it("should throw error on missing property", () => {
      let coughtError: unknown;
      try {
        new ObjectToSchemaParserModule(schemas).parse({ title: "my book" }, "book");
      } catch(error: unknown) {
        coughtError = error;
      }

      expect(coughtError).toBeInstanceOf(ParseError);
      expect((coughtError as ParseError).code).toEqual(parseError.PROPERTY_VALUE_NOT_FOUND);
      expect((coughtError as ParseError).schema?.name).toEqual("book");
      expect((coughtError as ParseError).property?.name).toEqual("author");
    });
    it("should throw error on excessive property", () => {
      let coughtError: unknown;
      try {
        new ObjectToSchemaParserModule(schemas).parse({ foo: "bar" }, "book");
      } catch(error: unknown) {
        coughtError = error;
      }

      expect(coughtError).toBeInstanceOf(ParseError);
      expect((coughtError as ParseError).code).toEqual(parseError.PROPERTY_NOT_EXISTS_IN_SCHEMA);
    });
  });
});