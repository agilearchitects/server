import { describe, it } from "@jest/globals";
import { httpParserFactory } from "../factories/http-parser.factory";
import { parseDTOFactory } from "../factories/parse-dto.factory";
import { routerFactory } from "../factories/router.factory";
import { HttpHandler, handle, Method } from "../http-handler";
import { methodHttpHandler } from "../http-handler/method.http-handler";
import { routeHttpHandler } from "../http-handler/route.http-handler";
import { schemas } from "./router.module";


describe("RouterModule", () => {
  const schemas: schemas = {
    book: {
      properties: {
        id: "number",
        title: "string",
        author: {
          type: "author",
          optional: true,
        }
      }
    },
    createBook: {
      properties: {
        title: "string",
        authorId: "number"
      }
    },
    updateBook: {
      properties: {
        id: "number",
        title: "string",
        authorId: "number"
      }
    },
    author: {
      properties: {
        id: "number",
        lastname: "string",
        firstname: {
          type: "string",
          optional: true,
        },
        books: {
          type: "book[]",
          optional: true
        }
      }
    },
    createAuthor: {
      properties: {
        lastname: "string",
        firstname: {
          type: "string",
          optional: true
        }
      }
    },
    updateAuthor: {
      properties: {
        id: "number",
        lastname: "string",
        firstname: {
          type: "string",
          optional: true
        }
      }
    }
  };
  interface IBook {
    id: number;
    title: string;
    authorId: number;
  }
  interface IAuthor {
    id: number;
    lastname: string;
    firstname?: string;
    books?: IBook[];
  }
  let books: IBook[] = [];
  let authors: IAuthor[] = [];

  const nextId = (list: ({ id: number})[]): number => list[list.length - 1].id + 1;
  const addBook = (payload: { title: string, authorId: number }): number => { const bookId = nextId(books); books.push({ id: bookId, ...payload }); return bookId; };
  const getBook = (id: number): { id: number, title: string } => { const book = books.find(book => book.id === id); if(book === undefined) { throw new Error(); } return { id: book.id, title: book.title }; };
  const updateBook = (payload: { id: number, title: string, authorId: number}) => { const bookIndex = books.findIndex(book => book.id === payload.id); if(bookIndex === -1) { throw new Error(); } books[bookIndex] = { ...books[bookIndex], title: payload.title, authorId: payload.authorId }; };
  const deleteBook = (id: number): void => { books = books.filter(book => book.id !== id); };
  
  const addAuthor = (payload: { lastname: string, firstname?: string }): number => { const authorId = nextId(authors); authors.push({ id: authorId, ...payload }); return authorId; };
  const getAuthor = (id: number): IAuthor => { const author = authors.find(author => author.id === id); if(author === undefined) { throw new Error(); } return author; };
  const updateAuthor = (payload: { id: number, lastname: string, firstname?: string}) => { const authorIndex = authors.findIndex(author => author.id === payload.id); if(authorIndex === -1) { throw new Error(); } authors[authorIndex] = { ...authors[authorIndex], firstname: payload.firstname, ...(payload.lastname !== undefined ? { lastname: payload.lastname } : undefined) }; };
  const deleteAuthor = (id: number): void => { authors = authors.filter(author => author.id !== id); };
  
  
  beforeEach(() => {
    books = [{ id: 1, title: "My Book", authorId: 1 }, { id: 2, title: "Second Book", authorId: 2 }];
    authors = [{ id: 1, lastname: "Doe" }, { id: 2, lastname: "Black", firstname: "Walter" }];
  });

  const b = httpParserFactory(parseDTOFactory);
  const { group, get, post, del, put} = routerFactory({ routeHandler: routeHttpHandler, methodHandler: methodHttpHandler, parseInputHandler: b.parseInput, parseOutputHandler: b.parseOutput }, schemas);

  const routes = group("", {}, [group("book", { name: "book" }, [
    get("", { name: "index", parseResponse: "book[]" }, (handler: HttpHandler) => handler.sendJson(books.map((book: IBook) => ({ id: book.id, title: book.title })))),
    get(":id", { name: "show", parseResponse: "book" }, (handler: HttpHandler) => handler.sendJson(getBook(parseInt(handler.getPayload<{ id: string }>("params", { id: "" }).id)))),
    post("", { name: "create", parseRequest: "createBook", parseResponse: "book" }, (handler: HttpHandler) => {
      const book = handler.getPayload<{ title: string, authorId: number }>("parsedBody");
      if(book === undefined) { throw new Error(); } 
      const newBookId = addBook(book); handler.sendJson(getBook(newBookId));
    }),
    del(":id", { name: "delete"}, (handler: HttpHandler) => { deleteBook(parseInt(handler.getPayload<{ id: string }>("params", { id: ""}).id)); handler.sendStatus(200); }),
    put(":id", { name: "update", parseRequest: "updateBook" }, (handler: HttpHandler) => { const book = handler.getPayload<{ id: number, title: string, authorId: number }>("parsedBody"); if(book === undefined) { throw new Error(); } updateBook(book); handler.sendJson(getBook(book.id)); })
  ]),
  group("author", { name: "author" }, [
    get("", { name: "index", parseResponse: "author[]" }, (handler: HttpHandler) => handler.sendJson(authors.map((author: IAuthor) => ({ id: author.id, lastname: author.lastname, ...(author.firstname !== undefined ? { firstname: author.firstname } : undefined) })))),
    get(":id", { name: "show", parseResponse: "author" }, (handler: HttpHandler) => handler.sendJson(getAuthor(parseInt(handler.getPayload<{ id: string }>("params", { id: ""}).id)))),
    post("", { name: "create", parseRequest: "createAuthor" }, (handler: HttpHandler) => { const author = handler.getPayload<{ lastname: string, firstname?: string }>("parsedBody"); if(author === undefined) { throw new Error(); } const authorId = addAuthor(author); handler.sendJson(getAuthor(authorId)); }),
    del("", { name: "delete"}, (handler: HttpHandler) => { deleteAuthor(parseInt(handler.getPayload<{ id: string }>("params", { id: ""}).id)); handler.sendStatus(200); }),
    put(":id", { name: "update", parseRequest: "updateAuthor" }, (handler: HttpHandler) => { const author = handler.getPayload<{ id: number, lastname: string, firstname?: string }>("parsedBody"); if(author === undefined) { throw new Error(); } updateAuthor(author); handler.sendJson(getAuthor(author.id)); })
  ])]);

  it("should return books", async () => {
    const result = await handle(Method.GET, "http://127.0.0.1/book", {}, null, routes.handler);
    const body = JSON.parse(result.body as string);
    expect(body).toHaveLength(2);
  });
  it("should return one book", async () => {
    const bookId: number = 1;
    const result = await handle(Method.GET, `http://127.0.0.1/book/${bookId}`, {}, null, routes.handler);
    const body = JSON.parse(result.body as string);
    expect(body.id).toEqual(bookId);
  });
  it("should add book", async () => {
    const bookTitle: string = "New book";
    const bookId = nextId(books);
    const result = await handle(Method.POST, "http://127.0.0.1/book", {}, JSON.stringify({ title: bookTitle, authorId: 1 }), routes.handler);
    const body = JSON.parse(result.body as string);
    expect(body.id).toEqual(bookId);
    expect(body.title).toEqual(bookTitle);
  });
  it("should update book", async () => {
    const newTitle = "New title";
    const bookId: number = 1;
    const result = await handle(Method.PUT, `http://127.0.0.1/book/${bookId}`, {}, JSON.stringify({ id: bookId, title: newTitle, authorId: 2 }), routes.handler);
    const body = JSON.parse(result.body as string);
    expect(body.id).toEqual(bookId);
    expect(body.title).toEqual(newTitle);
  });
  it("should delete book", async () => {
    const bookId: number = 1;
    const result = await handle(Method.DELETE, `http://127.0.0.1/book/${bookId}`, {}, null, routes.handler);
    expect(result.status).toEqual(200);
    expect(books).toHaveLength(1);
  });

  it("should aggregate all parser schema names from routes", () => {
    const schemas: string[] = routes.allParserSchemaNames;
    expect(schemas);
  });
});