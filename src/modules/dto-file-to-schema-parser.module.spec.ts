import { describe, it } from "@jest/globals";
import * as ts from "typescript";
import { DTOFileToSchemaParserModule } from "./dto-file-to-schema-parser.module";

describe("DTOFileToSchemaParserModule", () => {
  it("should parse file", () => {
    const dtoFileToSchemaModule = new DTOFileToSchemaParserModule("./test/dto", ts);
    const schema = dtoFileToSchemaModule.parseFile("author");
    expect(Object.keys(schema.properties)).toHaveLength(2);
    expect(schema.properties.lastname.optional).toBeUndefined();
    expect(schema.properties.firstname.optional).toEqual(true);
    expect(schema.properties.firstname.description).toEqual("Authors firstname (if exists)");
  });
  it("should parse schemas", () => {
    const dtoFileToSchemaModule = new DTOFileToSchemaParserModule("./test/dto", ts);
    const schemas = dtoFileToSchemaModule.schemasToSchemaInput(dtoFileToSchemaModule.getSchemasWithNames(["book"]));
    expect(schemas.book).toBeDefined();
    expect((schemas.book.properties.author as any).type).toEqual("author[]");
    expect(schemas.author).toBeDefined();
    expect((schemas.author.properties.lastname as any).type).toEqual("string");
  });
});