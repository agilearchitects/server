import * as fs from "fs";
import { createRouter, HttpHandler } from "../lib";
import { AuthorController } from "./controllers/author.controller";
import { BookController } from "./controllers/book.controller";
import { VersionController } from "./controllers/version.controller";

const versionController = new VersionController();
const bookController = new BookController();
const authorController = new AuthorController();

const schemas = JSON.parse(fs.readFileSync(`${__dirname}/schema.json`, "utf8"));
const route = createRouter(schemas);

export const router = route.group("", {}, [
  route.get("version", { name: "version", parseResponse: "version" }, versionController.index()),
  route.group("book", { name: "book" }, [
    route.get("", { name: "index", parseResponse: "book[]" }, bookController.index()),
    route.get(":id(^[\\d+]$)", { name: "show", parseResponse: "book" }, bookController.show()),
    route.post("", { name: "create", parseRequest: "book-create", parseResponse: "book" }, bookController.create()),
    route.put(":id(^[\\d+]$)", { name: "update", parseRequest: "book-update", parseResponse: "book" }, bookController.update()),
    route.del(":id(^[\\d+]$)", { name: "delete" }, bookController.delete()),
  ]),
  route.group("author", { name: "author" }, [
    route.get("", { name: "index", parseResponse: "author[]" }, authorController.index()), 
    route.get(":id(^[\\d+]$)", { name: "show", parseResponse: "author" }, authorController.show())
  ]),
  route.get("documentation", { name: "documentation" }, (handler: HttpHandler) => handler.sendJson(schemas))
]);