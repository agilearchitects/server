import * as fs from "fs";
import * as typescript from "typescript";
import { routerToSchemas } from "../lib/server";
import { router } from "./router";

const schemas = routerToSchemas(`${__dirname}/dto`, router, typescript);
fs.writeFileSync(`${__dirname}/schema.json`, JSON.stringify(schemas, null, 4), "utf8");
console.log("Schema has been updated");