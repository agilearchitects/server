export interface IVersionDTO {
  // Version using semver standard
  version: string;
}