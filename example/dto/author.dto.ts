export interface IAuthorDTO {
  id: number;
  lastname: string;
  firstname?: string;
}