export interface IBookUpdateDTO {
  title: string;
  ISBN: string;
  authorId: number[];
}