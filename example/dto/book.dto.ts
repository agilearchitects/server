import { IAuthorDTO } from "./author.dto";

export interface IBookDTO {
  id: number;
  title: string;
  ISBN: string;
  author: IAuthorDTO[]
}