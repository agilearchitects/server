export interface IBookCreateDTO {
  title: string;
  ISBN: string;
  authorId: number[];
}