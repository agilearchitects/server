type config = {
  port: number,
  apiHost: string,
  spaHost: string, 
  cors: RegExp;
}
export const getConfig = (): config => {
  const spaHost = process.env.SPA_HOST || "www.test.test";
  return {
    port: parseInt(process.env.PORT || "1234"),
    apiHost: process.env.API_HOST || "api.test.test",
    spaHost,
    cors: new RegExp(`^http[s]?://${spaHost}(?::[0-9]+|)$`)
  };
};