import { createServer, staticContent, vhost, cors } from "../lib";
import { getConfig } from "./config";
import { router } from "./router";

const { port, apiHost, spaHost, cors: corsRegExp} = getConfig();

const server = createServer(
  vhost(apiHost, cors(corsRegExp), router.handler),
  vhost(spaHost, staticContent("./example/static")),
);
server.listen(port, () => {
  console.log(`API listening on http://${apiHost}:${port}`);
  console.log(`SPA listening on http://${spaHost}:${port}`);
});
server.onError((error: unknown) => {
  console.log("ON ERROR", error);
});