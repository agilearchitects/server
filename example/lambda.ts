import { createLambda, cors, vhost, IAPIGatewayProxyEvent, IAPIGatewayProxyResult } from "../lib";
import { getConfig } from "./config";
import { router } from "./router";

const { apiHost, cors: corsRegExp} = getConfig();

// Create server
const server = createLambda(vhost(apiHost, cors(corsRegExp), router.handler));
export const handler = async (event: IAPIGatewayProxyEvent): Promise<IAPIGatewayProxyResult> => {
  return await server.handler(event);
};
