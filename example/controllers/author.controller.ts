import { HttpHandler, httpHandlerMethod } from "../../lib";
import { IAuthorDTO } from "../dto/author.dto";

const authors: IAuthorDTO[] = [
  { id: 1, lastname: "Doe" },
  { id: 2, firstname: "Joe", lastname: "Smith" }
];

export class AuthorController {
  public index = (): httpHandlerMethod => (handler: HttpHandler) => {
    handler.sendJson(authors);
  };

  public show = (): httpHandlerMethod => (handler: HttpHandler) => {
    const author: IAuthorDTO | undefined = authors.find((author: IAuthorDTO) => author.id === parseInt(handler.getPayload<{ id: string }>("params", { id: ""}).id));
    if(author !== undefined) {
      handler.sendJson(author);
    } else {
      handler.sendStatus(404);
    }
  };
}