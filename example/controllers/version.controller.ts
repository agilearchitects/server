import { HttpHandler, httpHandlerMethod } from "../../lib";

export class VersionController {
  public index = (): httpHandlerMethod => (handler: HttpHandler) => {
    handler.sendJson({ version: "1" } );
  };
}