import { HttpHandler, httpHandlerMethod } from "../../lib";
import { IAuthorDTO } from "../dto/author.dto";
import { IBookCreateDTO } from "../dto/book-create.dto";
import { IBookUpdateDTO } from "../dto/book-update.dto";
import { IBookDTO } from "../dto/book.dto";

const authors: IAuthorDTO[] = [
  { id: 1, lastname: "Doe" },
  { id: 2, firstname: "Joe", lastname: "Smith" }
];
let books: IBookDTO[] = [
  {
    id: 1,
    title: "New World",
    ISBN: "1111111",
    author: [
      authors[0],
      authors[1],
    ]
  }, {
    id: 2,
    title: "My first book",
    ISBN: "222222",
    author: [
      authors[0]
    ]
  }
];

export class BookController {
  public index = (): httpHandlerMethod => (handler: HttpHandler) => {
    handler.sendJson(books);
  };
  public show = (): httpHandlerMethod => (handler: HttpHandler) => {
    const book: IBookDTO | undefined = books.find((book: IBookDTO) => book.id === parseInt(handler.getPayload<{ id: string }>("params", { id: "-1" }).id));
    if(book !== undefined) {
      handler.sendJson(book);
    } else {
      handler.sendStatus(404);
    }
  };
  public create = (): httpHandlerMethod => (handler: HttpHandler) => {
    const newBook: IBookCreateDTO | undefined = handler.getPayload<IBookCreateDTO>("parsedBody");
    if(newBook === undefined) {
      handler.sendStatus(500);
      return;
    }

    const addAuthors: IAuthorDTO[] = newBook.authorId.map((id: number) => {
      const author: IAuthorDTO | undefined = authors.find((author: IAuthorDTO) => author.id === id);
      if(author === undefined) {
        throw true;
      }
      return author;
    });

    let nextId: number;
    try {
      nextId = books.sort((a: IBookDTO, b: IBookDTO) => a.id < b.id ? 1 : -1)[0].id + 1;
    } catch {
      nextId = 1;
    }
    const addBook: IBookDTO = {
      id: nextId,
      title: newBook.title,
      ISBN: newBook.ISBN,
      author: addAuthors,
    };
    books.push(addBook);

    handler.sendJson(addBook);
  };
  public update = (): httpHandlerMethod => (handler: HttpHandler) => {
    const updateBook: IBookUpdateDTO | undefined = handler.getPayload<IBookUpdateDTO>("parsedBody");
    const existingBook: IBookDTO | undefined = books.find((book: IBookDTO) => book.id === parseInt(handler.getPayload<{ id: string }>("params", { id: "-1"}).id));
    if(updateBook === undefined || existingBook == undefined) {
      handler.sendStatus(500);
      return;
    }
    
    let updateAuthors: IAuthorDTO[];
    try {
      updateAuthors = updateBook.authorId.map((id: number) => {
        const author: IAuthorDTO | undefined = authors.find((author: IAuthorDTO) => author.id === id);
        if(author === undefined) {
          throw true;
        }
        return author;
      });
    } catch {
      handler.sendStatus(500);
      return;
    }

    const newBook = {
      id: existingBook.id,
      title: updateBook.title,
      ISBN: updateBook.ISBN,
      author: updateAuthors,
    };
    
    books = [
      ...books.filter((book: IBookDTO) => book.id !== existingBook.id),
      newBook
    ];

    return handler.sendJson(newBook);
  };
  public delete = (): httpHandlerMethod => (handler: HttpHandler) => {
    const deleteBook: IBookDTO | undefined = books.find((book: IBookDTO) => book.id === parseInt(handler.getPayload<{ id: string }>("params", { id: "" }).id));
    if(deleteBook === undefined) {
      handler.sendStatus(404);
      return;
    }
    books = [...books.filter((book: IBookDTO) => book.id !== deleteBook.id)];
    handler.sendStatus(200);
  };
}